$(document).ready(function(){

    // initialize default variables
	var xmlhttp = new XMLHttpRequest();
    var url = "js/images.json";
    var prev;
    var sliderWidth = 500;
    var width;
    var main = 0;
    var next = 1;

    
    $('#submit').click(function(){
        sliderWidth = $('#slider-width').val();
        if(sliderWidth > 199 && sliderWidth < 601){
            window.location.href = '?width=' + sliderWidth;
        } else {
            $('form').append('<p>You must enter a value between 200 and 600</p>');
        }
    })

    // Ajax connection
    function ajaxCall(){
        xmlhttp.onreadystatechange = function(){

        	if(xmlhttp.readyState==4 && xmlhttp.status==200){
    			arrayImages = JSON.parse(xmlhttp.responseText);
                
                if(prev == undefined){
                    prev = arrayImages.length - 1;
                }

                output(arrayImages);
    		} 
        }

        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    ////////////////////

    // loop logic for the next button
    function loopNext(jsonContent){

        prev ++
        main ++
        next ++

        if(next == arrayImages.length){
            next = 0;
        }
        else if(main == arrayImages.length){
            main = 0;
        }
        else if(prev == arrayImages.length){
            prev = 0; 
        }
    }

    // loop logic for the previous button
    function loopPrev(jsonContent){ 

        if(next == 0){
            next = arrayImages.length;
        }
        else if(main == 0){
            main = arrayImages.length;
        }
        else if(prev == 0){
            prev = arrayImages.length; 
        }

        prev --
        main --
        next --
    }

    // right button
    $('#arrow-prev').click(function(){

        move = width + 'px';
        
        $(".wrap-img img").animate(
            {left: move},{duration:1000, specialEasing:{left: "easeInOutExpo"},complete: function() {
                ajaxCall();
            }
        });  

        loopPrev(arrayImages);
    })

    // next button
    $('#arrow-next').click(function(){

        move = width + 'px';
        
        $(".wrap-img img").animate(
            {right: move},{duration:1000, specialEasing:{right: "easeInOutExpo"},complete: function() {
                ajaxCall();
            }
        });

        loopNext(arrayImages);

    })

    // function that prints the sliding pictures that are loaded by the ajax call
    function output(jsonContent){

        result = '';
        result += ('<img src="images/' + jsonContent[prev] + '.jpg" alt="images" />');
        result += ('<img src="images/' + jsonContent[main] + '.jpg" alt="images" />');
        result += ('<img src="images/' + jsonContent[next] + '.jpg" alt="images" />');

        $('.wrap-img').html(result);

        if(!location.search.substring(1)){
            width = sliderWidth;
        }
        else{
            var queryString = location.search.substring(1);
            var regex = /\d+/;
            var match = queryString.match(regex);
            width = match;
        }

        $('.wrap-img img').css({'width': width}); 
        $('.wrap-img').css({'width': (width * 3), 'left': - width});
        $('.wrapper').css({'width': width});

    }

    // load the default output
    ajaxCall();

});